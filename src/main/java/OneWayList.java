import java.util.*;


public class OneWayList<T> implements List<T> {

    private static class Node<T> {

        T value;
        Node<T> next;

        Node(T value) {
            this.value = value;
        }

    }

    private class OneWayListIterator implements Iterator<T> {

        private Node<T> current = head.next;

        @Override
        public boolean hasNext() {
            return current != tail;
        }

        @Override
        public T next() {

            T value = current.value;

            current = current.next;

            return value;

        }

    }

    private int size = 0;

    private final Node<T> head = new Node<>(null); // Dummy head
    private final Node<T> tail = new Node<>(null); // Dummy tail

    public OneWayList() {
        this.head.next = this.tail;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.head.next == this.tail;
    }

    @Override
    public boolean contains(Object o) {

        for (T t : this)
            if (Objects.equals(t, o))
                return true;

        return false;

    }

    @Override
    public Iterator<T> iterator() {
        return new OneWayListIterator();
    }

    @Override
    public Object[] toArray() {

        Object[] array = new Object[this.size()];

        int i = 0;

        for (T t : this)
            array[i++] = t;

        return array;

    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) this.toArray();
    }

    @Override
    public boolean add(T t) {

        Node<T> current = this.head;

        while (current.next != this.tail)
            current = current.next;

        current.next = new Node<T>(t);
        current.next.next = this.tail;

        this.size++;

        return true;

    }

    @Override
    public boolean remove(Object o) {

        Node<T> current = this.head;

        while (!Objects.equals(current.next.value, o) && current.next != this.tail)
            current = current.next;

        if (current.next != this.tail) {

            current.next = current.next.next;

            this.size--;

            return true;

        }

        return false;

    }

    @Override
    public boolean containsAll(Collection<?> c) {

        for (Object o : c)
            if (!this.contains(o))
                return false;

        return true;

    }

    @Override
    public boolean addAll(Collection<? extends T> c) {

        if (c.isEmpty())
            return false;

        Node<T> current = this.head;

        while (current.next != this.tail)
            current = current.next;

        for (T t : c) {
            current.next = new Node<>(t);
            current = current.next;
        }

        current.next = this.tail;

        this.size += c.size();

        return true;

    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {

        if (index == this.size())
            return this.addAll(c);

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        if (c.isEmpty())
            return false;

        Node<T> current;
        Node<T> tmp;

        if (index == 0) {

            current = this.head;
            tmp = this.head.next;

        } else {

            current = this.findNode(index - 1);
            tmp = current.next;

        }

        for (T t : c) {
            current.next = new Node<>(t);
            current = current.next;
        }

        current.next = tmp;

        this.size += c.size();

        return true;

    }

    @Override
    public boolean removeAll(Collection<?> c) {

        boolean removed = false;

        Node<T> current = this.head;

        while (current.next != this.tail) {
            if (c.contains(current.next.value)) {
                current.next = current.next.next;
                this.size--;
                removed = true;
            } else current = current.next;
        }

        return removed;

    }

    @Override
    public boolean retainAll(Collection<?> c) {

        boolean removed = false;

        Node<T> current = this.head;

        while (current.next != this.tail) {
            if (!c.contains(current.next.value)) {
                current.next = current.next.next;
                this.size--;
                removed = true;
            } else current = current.next;
        }

        return removed;

    }

    @Override
    public void clear() {

        this.head.next = this.tail;

        this.size = 0;

    }

    @Override
    public T get(int index) {

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        return this.findNode(index).value;

    }

    @Override
    public T set(int index, T element) {

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        Node<T> current = this.findNode(index);

        T old = current.value;

        current.value = element;

        return old;

    }

    @Override
    public void add(int index, T element) {

        if (index == this.size()) {
            this.add(element);
            return;
        }

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        Node<T> current;
        Node<T> tmp;

        if (index == 0) {

            tmp = this.head.next;
            this.head.next = new Node<>(element);
            this.head.next.next = tmp;

        } else {

            current = this.findNode(index - 1);
            tmp = current.next;
            current.next = new Node<>(element);
            current.next.next = tmp;

        }

        this.size++;

    }

    @Override
    public T remove(int index) {

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        T result;

        Node<T> current;

        if (index == 0) {
            result = this.head.next.value;
            this.head.next = this.head.next.next;
        } else {
            current = this.findNode(index - 1);
            result = current.next.value;
            current.next = current.next.next;
        }

        this.size--;

        return result;

    }

    @Override
    public int indexOf(Object o) {

        int i = 0;

        for (T t : this) {

            if (Objects.equals(t, o))
                return i;

            i++;

        }

        return -1;
        
    }

    @Override
    public int lastIndexOf(Object o) {

        int result = -1;

        int i = 0;

        for (T t : this) {

            if (Objects.equals(t, o))
                result = i;

            i++;

        }

        return result;

    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {

        if (!this.validateIndex(fromIndex))
            throw new IndexOutOfBoundsException(fromIndex);

        if (toIndex != this.size() && !this.validateIndex(toIndex))
            throw new IndexOutOfBoundsException(fromIndex);

        if (fromIndex > toIndex)
            throw new IllegalArgumentException("fromIndex > toIndex");

        List<T> result = new OneWayList<>();

        Node<T> current = this.findNode(fromIndex);

        for (int i = fromIndex; i < toIndex; i++) {
            result.add(current.value);
            current = current.next;
        }

        return result;
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder("OneWayList: [");

        for (T t : this)
            result.append(t.toString()).append(", ");

        if (this.isEmpty())
            result.append("]");
        else result.replace(result.length() - 2, result.length(), "]");

        return result.toString();

    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;

        if (!(obj instanceof OneWayList))
            return false;

        OneWayList<?> list = (OneWayList<?>) obj;

        if (this.size() != list.size())
            return false;

        Iterator<?> iterator = list.iterator();

        for (T t : this)
            if (!Objects.equals(t, iterator.next()))
                return false;

        return true;

    }

    private Node<T> findNode(int index) {

        Node<T> current = this.head.next;

        int i = 0;

        while (i < index) {
            current = current.next;
            i++;
        }

        return current;

    }

    private boolean validateIndex(int index) {
        return index >= 0 && index < this.size;
    }

}
