import java.util.*;


public class DoubleLinkedList<T> implements List<T> {

    private static class Node<T> {

        T value;

        Node<T> prev;
        Node<T> next;

        Node(T value) {
            this.value = value;
        }

    }

    private class DoubleLinkedListIterator implements Iterator<T> {

        private Node<T> current = head.next;

        @Override
        public boolean hasNext() {
            return current != tail;
        }

        @Override
        public T next() {

            T value = current.value;

            current = current.next;

            return value;

        }

    }

    private int size = 0;

    private final Node<T> head = new Node<>(null); // Dummy head
    private final Node<T> tail = new Node<>(null); // Dummy tail

    public DoubleLinkedList() {

        this.head.next = this.tail;
        this.tail.prev = this.head;

    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.head.next == this.tail;
    }

    @Override
    public boolean contains(Object o) {

        for (T t : this)
            if (Objects.equals(t, o))
                return true;

        return false;

    }

    @Override
    public Iterator<T> iterator() {
        return new DoubleLinkedListIterator();
    }

    @Override
    public Object[] toArray() {

        Object[] result = new Object[this.size()];

        int i = 0;

        for (T t : this)
            result[i++] = t;

        return result;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) this.toArray();
    }

    @Override
    public boolean add(T t) {

        Node<T> node = new Node<>(t);

        node.next = this.tail;
        node.prev = this.tail.prev;

        this.tail.prev.next = node;
        this.tail.prev = node;

        this.size++;

        return true;

    }

    @Override
    public boolean remove(Object o) {

        Node<T> current = this.head.next;

        while (!Objects.equals(current.value, o) && current != this.tail)
            current = current.next;

        if (current != this.tail) {

            current.prev.next = current.next;
            current.next.prev = current.prev;

            this.size--;

            return true;

        }

        return false;

    }

    @Override
    public boolean containsAll(Collection<?> c) {

        for (Object o : c)
            if (!this.contains(o))
                return false;

        return true;

    }

    @Override
    public boolean addAll(Collection<? extends T> c) {

        if (c.isEmpty())
            return false;

        for (T t : c)
            this.add(t);

        return true;

    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {

        if (index != this.size() && !validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        if (c.isEmpty())
            return false;

        Node<T> current = this.findNode(index).prev;

        Node<T> tmp = current.next;

        for (T t : c) {

            current.next = new Node<>(t);
            current.next.prev = current;

            current = current.next;

        }

        current.next = tmp;
        tmp.prev = current;

        this.size += c.size();

        return true;

    }

    @Override
    public boolean removeAll(Collection<?> c) {

        boolean removed = false;

        Node<T> current = this.head.next;

        while (current != this.tail) {

            if (c.contains(current.value)) {

                current.prev.next = current.next;
                current.next.prev = current.prev;

                this.size--;

                removed = true;

            }

            current = current.next;

        }

        return removed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        boolean removed = false;

        Node<T> current = this.head.next;

        while (current != this.tail) {

            if (!c.contains(current.value)) {

                current.prev.next = current.next;
                current.next.prev = current.prev;

                this.size--;

                removed = true;

            }

            current = current.next;

        }

        return removed;

    }

    @Override
    public void clear() {

        this.head.next = this.tail;
        this.tail.prev = this.head;

        this.size = 0;

    }

    @Override
    public T get(int index) {

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        return this.findNode(index).value;

    }

    @Override
    public T set(int index, T element) {

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        Node<T> current = this.findNode(index);

        T old = current.value;

        current.value = element;

        return old;
    }

    @Override
    public void add(int index, T element) {

        if (index != this.size() && !this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        Node<T> current = this.findNode(index);

        current.prev.next = new Node<>(element);
        current.prev.next.next = current;
        current.prev.next.prev = current.prev;
        current.prev = current.prev.next;

        this.size++;

    }

    @Override
    public T remove(int index) {

        if (!this.validateIndex(index))
            throw new IndexOutOfBoundsException(index);

        Node<T> current = this.findNode(index);

        current.prev.next = current.next;
        current.next.prev = current.prev;

        this.size--;

        return current.value;

    }

    @Override
    public int indexOf(Object o) {

        int i = 0;

        for (T t : this)
            if (Objects.equals(t, o))
                return i;
            else i++;

        return -1;

    }

    @Override
    public int lastIndexOf(Object o) {

        int i = this.size() - 1;

        Node<T> current = this.tail.prev;

        while (current != head) {

            if (Objects.equals(current.value, o))
                return i;

            current = current.prev;

            i--;

        }

        return -1;

    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {

        if (!this.validateIndex(fromIndex))
            throw new IndexOutOfBoundsException(fromIndex);

        if (toIndex != this.size() && !this.validateIndex(toIndex))
            throw new IndexOutOfBoundsException(fromIndex);

        if (fromIndex > toIndex)
            throw new IllegalArgumentException("fromIndex > toIndex");

        List<T> result = new DoubleLinkedList<>();

        Node<T> current = this.findNode(fromIndex);

        for (int i = fromIndex; i < toIndex; i++) {
            result.add(current.value);
            current = current.next;
        }

        return result;

    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder("DoubleLinkedList: [");

        for (T t : this)
            result.append(t.toString()).append(", ");

        if (this.isEmpty())
            result.append("]");
        else result.replace(result.length() - 2, result.length(), "]");

        return result.toString();

    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;

        if (!(obj instanceof DoubleLinkedList))
            return false;

        DoubleLinkedList<?> list = (DoubleLinkedList<?>) obj;

        if (this.size() != list.size())
            return false;

        Iterator<?> iterator = list.iterator();

        for (T t : this)
            if (!Objects.equals(t, iterator.next()))
                return false;

        return true;

    }

    private Node<T> findNode(int index) {

        int i;

        Node<T> current;

        if (index < this.size() / 2) {

            i = 0;

            current = this.head.next;

            while (i < index) {
                current = current.next;
                i++;
            }

        } else {

            i = this.size();

            current = this.tail;

            while (i > index) {
                current = current.prev;
                i--;
            }

        }

        return current;

    }

    private boolean validateIndex(int index) {
        return index >= 0 && index < this.size;
    }

}
